const mongoose = require('mongoose')

const Posts = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        default: "nothing"
    },
    Author: {
        type: String,
        required: true
    },
    usersId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Users",
        required: true
    },
})

module.exports = mongoose.model("Products", Posts)