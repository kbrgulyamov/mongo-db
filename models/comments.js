const mongoose = require("mongoose")

const Comments = mongoose.Schema({
      Author: {
            type: mongoose.Schema.Types.ObjectId,
            required: true
      },
      Posts: {
            type: mongoose.Schema.Types.ObjectId,
            required: true
      },
      text: {
            type: String,
            required: true
      }
})


module.exports = mongoose.model("Comments", Comments)