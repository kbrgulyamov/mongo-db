const mongoose = require('mongoose')

const Users = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    Surname: {
        type: String,
        required: true
    },
    password: {
        type: Number,
        required: true
    },
    GenderMale: {
        type: Boolean,
        required: false
    },
    Posts: {
        type: mongoose.Schema.Types.Array,
        ref: "Posts",
    },
    Descriptions: {
        type: String,
        required: true
    }
})

module.exports = mongoose.model("Users", Users) 